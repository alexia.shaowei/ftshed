package main

import (
	"fmt"

	cf "future.net.co.ftsched/conf"
	sq "future.net.co.ftsched/sqlscript"
	db "gitlab.com/alexia.shaowei/swmysql"
)

func createTable() error {

	dbdes, err := db.New(dbDes.User, dbDes.Password, dbDes.Host, dbDes.Port, cstSuperset, dbDes.Charset, 1, 1)
	if err != nil {
		return fmt.Errorf("createTable, new db error: %v", err)
	}
	defer dbdes.Close()

	sqlSyntax := sq.CreateTableGamesPerHour() + sq.CreateTableUsersPerHour() + sq.CreateTableGameMonthAVG()

	_, err = dbdes.Exec(sqlSyntax)
	if err != nil {
		return fmt.Errorf("createTable, create table error: %v", err)
	}

	return nil
}

func run(dbSrcCfg cf.DBSrcCfg) error {
	dbdes, err := db.New(dbDes.User, dbDes.Password, dbDes.Host, dbDes.Port, cstSuperset, dbDes.Charset, 1, 1)
	if err != nil {
		return err
	}

	dbsrc, err := db.New(dbSrcCfg.User, dbSrcCfg.Password, dbSrcCfg.Host, dbSrcCfg.Port, cstDBDefault, dbSrcCfg.Charset, 1, 1)
	if err != nil {
		return err
	}

	defer func() {
		dbdes.Close()
		dbsrc.Close()

		l.Info("dbdes, dbsrc disconnection")
	}()

	if err := transGameData(dbSrcCfg, dbdes, dbsrc); err != nil {
		l.Error("transGameData error: %v", err)
	}

	if err := transRegUsersData(dbSrcCfg, dbdes, dbsrc); err != nil {
		l.Error("transRegUsersData error: %v", err)
	}

	if err := transGameMAVGData(dbSrcCfg, dbdes, dbsrc); err != nil {
		l.Error("transGameMAVGData error: %v", err)
	}

	return nil
}

// func massiveData() error {
// 	for _, srcDB := range cfg.RDBCfg.Source {
// 		if srcDB.Enable {
// 			whatTheMess(srcDB)
// 		}
// 	}

// 	return nil
// }
