package tmparse

import (
	"fmt"
	"strconv"
	"time"
)

func TimeFMTCheck(in, timeFMT string) bool {
	_, err := time.Parse(timeFMT, in)

	return err == nil
}

func TimeCurrent(timeFMT string) string {
	t := time.Now()

	return t.Format(timeFMT)
}

func TimeUnixCompare(t1, t2, timeFMT string) int {
	tf1, _ := time.Parse(timeFMT, t1)
	tf2, _ := time.Parse(timeFMT, t2)

	uxT1 := tf1.Unix()
	uxT2 := tf2.Unix()

	if uxT1 == uxT2 {
		return 0
	}

	if uxT1 > uxT2 {
		return 1
	}

	return -1
}

func TimeInterval(t string) (string, string) {
	tf, _ := time.Parse(CST_TIME_FMT_YMDHMS, t)
	t1 := tf.Format(CST_TIME_FMT_YMDH)

	return t1 + CST_TIME_TEMP_SUFFIX_00, t1 + CST_TIME_TEMP_SUFFIX_59
}

func TimeOnTheHour(t, timeFMT string) string {
	tm, _ := time.Parse(timeFMT, t)
	return tm.Format(CST_TIME_FMT_YMDH) + CST_TIME_TEMP_SUFFIX_00
}

func TimeFMT(t, intimeFMT, outtimeFMT string) string {
	tm, _ := time.Parse(intimeFMT, t)

	return tm.Format(outtimeFMT)
}

func TimeOnTheHourNext(t, timeFMT string, addHours int) string {
	tm, _ := time.Parse(timeFMT, t)
	tm = tm.Add(time.Hour * time.Duration(addHours))

	return tm.Format(CST_TIME_FMT_YMDH) + CST_TIME_TEMP_SUFFIX_00
}

func TimeNext(t, timeFMT string, add int) string {
	tm, _ := time.Parse(timeFMT, t)
	tm = tm.Add(time.Hour * time.Duration(add))

	return tm.Format(timeFMT)
}

func TimeAdd(t, timeFMT string, add time.Duration) string {
	tm, _ := time.Parse(timeFMT, t)
	tm = tm.Add(add)

	return tm.Format(timeFMT)
}

func TimeOnTheMonthInterval(headTm, tailTm, inTimeFMT, outTimeFMT string) ([]string, error) {
	if !TimeFMTCheck(headTm, inTimeFMT) {
		return make([]string, 0), fmt.Errorf("tmparse check timeFMT error, headTm: %v, timeFMT: %v", headTm, inTimeFMT)
	}

	if !TimeFMTCheck(tailTm, inTimeFMT) {
		return make([]string, 0), fmt.Errorf("tmparse check timeFMT error, tailTm: %v, timeFMT: %v", tailTm, inTimeFMT)
	}

	headT := TimeFMT(headTm, inTimeFMT, outTimeFMT)
	tailT := TimeFMT(tailTm, inTimeFMT, outTimeFMT)
	curtT := TimeCurrent(outTimeFMT)

	if TimeUnixCompare(headT, curtT, outTimeFMT) == 0 {
		return []string{curtT}, nil
	}

	if TimeUnixCompare(headT, tailT, outTimeFMT) > 0 {
		return make([]string, 0), fmt.Errorf("tmparse get month interval error: headT is greater than tailT")
	}

	if TimeUnixCompare(tailT, curtT, outTimeFMT) > 0 {
		tailT = curtT
	}

	ils := make([]string, 0)

	for TimeUnixCompare(headT, tailT, outTimeFMT) <= 0 {
		ils = append(ils, headT)

		headT = TimeAdd(headT, outTimeFMT, time.Hour*24*32)
	}

	return ils, nil
}

func TimeOnTheHourDayInterval(headTm, tailTm, timeFMT string) ([]map[string]string, error) {
	if !TimeFMTCheck(headTm, timeFMT) {
		return make([]map[string]string, 0), fmt.Errorf("tmparse check timeFMT error, headTm: %v, timeFMT: %v", headTm, timeFMT)
	}

	if !TimeFMTCheck(tailTm, timeFMT) {
		return make([]map[string]string, 0), fmt.Errorf("tmparse check timeFMT error, tailTm: %v, timeFMT: %v", tailTm, timeFMT)
	}

	headT := TimeOnTheHour(headTm, timeFMT)
	tailT := TimeOnTheHour(tailTm, timeFMT)
	curtT := TimeCurrent(CST_TIME_FMT_YMDHMS)

	if TimeUnixCompare(tailT, curtT, CST_TIME_FMT_YMDHMS) >= 0 {
		tailT = TimeOnTheHour(curtT, CST_TIME_FMT_YMDHMS)
	}

	if TimeUnixCompare(headT, tailT, CST_TIME_FMT_YMDHMS) > 0 {
		return make([]map[string]string, 0), fmt.Errorf("tmparse get day interval error: headT is greater than tailT")
	}

	ils := make([]map[string]string, 0)
	for TimeUnixCompare(headT, tailT, CST_TIME_FMT_YMDHMS) < 0 {
		h, t := TimeInterval(headT)
		ils = append(ils, map[string]string{
			"h": h,
			"t": t,
		})

		headT = TimeOnTheHourNext(headT, CST_TIME_FMT_YMDHMS, 1)
	}

	return ils, nil
}

func TimeOnTheDayInterval(headTm, tailTm string, inFMT, outFMT string) ([]string, error) {

	if !TimeFMTCheck(headTm, inFMT) {
		return make([]string, 0), fmt.Errorf("tmparse check timeFMT error, headTm: %v, timeFMT: %v", headTm, inFMT)
	}

	if !TimeFMTCheck(tailTm, inFMT) {
		return make([]string, 0), fmt.Errorf("tmparse check timeFMT error, tailTm: %v, timeFMT: %v", tailTm, inFMT)
	}

	headT := TimeFMT(headTm, inFMT, outFMT)
	tailT := TimeFMT(tailTm, inFMT, outFMT)
	curtT := TimeCurrent(outFMT)

	if TimeUnixCompare(tailT, curtT, outFMT) >= 0 {
		tailT = TimeFMT(curtT, outFMT, outFMT)
	}

	if TimeUnixCompare(headT, curtT, outFMT) > 0 {
		return make([]string, 0), fmt.Errorf("tmparse get by interval error: headT is greater than current time")
	}

	if TimeUnixCompare(headT, tailT, outFMT) > 0 {
		return make([]string, 0), fmt.Errorf("tmparse get day interval error: headT is greater than tailT")
	}

	days := make([]string, 0)
	for TimeUnixCompare(headT, tailT, outFMT) <= 0 {
		days = append(days, headT)
		headT = TimeNext(headT, outFMT, 24)
	}

	return days, nil
}

func TimeGetYM(tm, timeFMT string) (string, string) {
	t, _ := time.Parse(timeFMT, tm)

	year := fmt.Sprintf("%04d", t.Year())
	month := fmt.Sprintf("%02d", int(t.Month()))

	return year, month
}

func TimeGetDayInterval(tm, timeFMT string) ([]map[string]string, error) {
	if !TimeFMTCheck(tm, timeFMT) {
		return make([]map[string]string, 0), fmt.Errorf("tmparse check timeFMT error, tm: %v, timeFMT: %v", tm, timeFMT)
	}

	h := TimeFMT(tm, timeFMT, CST_TIME_FMT_YMDHMS)
	t := TimeOnTheHourNext(h, CST_TIME_FMT_YMDHMS, 24)

	til, _ := TimeOnTheHourDayInterval(h, t, CST_TIME_FMT_YMDHMS)

	return til, nil
}

func TimeMonthDays(month, year string) (int, error) {
	m, err := strconv.Atoi(month)
	if err != nil {
		return 0, fmt.Errorf("TimeMonthDays input month(%v) error: %v", month, err)
	}

	y, err := strconv.Atoi(year)
	if err != nil {
		return 0, fmt.Errorf("TimeMonthDays input year(%v) error: %v", year, err)
	}

	return time.Date(y, time.Month(m)+1, 0, 0, 0, 0, 0, time.UTC).Day(), nil
}
