package main

import (
	"fmt"

	tm "future.net.co.ftsched/tmparse"
)

func ExampleZ() {
	h := "2022-01-04 09:00:00"
	h = tm.TimeNext(h, tm.CST_TIME_FMT_YMDHMS, 1)
	t := tm.TimeCurrent(tm.CST_TIME_FMT_YMDHMS)
	fmt.Printf("h: %v, t: %v\n", h, t)
	ti, err := tm.TimeOnTheHourDayInterval(h, t, tm.CST_TIME_FMT_YMDHMS)
	if err != nil {
		fmt.Printf("TimeOnTheHourDayInterval, error: %v\n", err)
	}

	for _, v := range ti {
		fmt.Printf("head: %v, tail: %v\n", v["h"], v["t"])
	}
}

func main() {
	h := "2022-01-28 09:00:00"
	e := "2022-11-04 09:00:00"

	ils, _ := tm.TimeOnTheMonthInterval(h, e, tm.CST_TIME_FMT_YMDHMS, tm.CST_TIME_FMT_YM)
	for _, month := range ils {
		fmt.Printf("month: %s\n", month)
	}

}
