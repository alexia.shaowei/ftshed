module future.net.co.ftsched

go 1.16

require (
	github.com/go-yaml/yaml v2.1.0+incompatible
	github.com/robfig/cron/v3 v3.0.1
	gitlab.com/alexia.shaowei/rotatelogs v0.0.0
	gitlab.com/alexia.shaowei/swmysql v0.0.2
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
