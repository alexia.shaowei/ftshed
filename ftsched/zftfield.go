package main

import (
	"errors"
	"fmt"
	"log"

	cf "future.net.co.ftsched/conf"
	lg "gitlab.com/alexia.shaowei/rotatelogs"
)

func init() {
	l, errg = lg.New(lg.SYS_WINDOWS, lg.MODE_DEBUG, 100, "./log/")
	if errg != nil {
		log.Fatalf("init logger error: %v", errg)
	}
}

func bootCfg() error {
	cfg = cf.ConfigYML{}

	err := cf.LoadServerConfig(CONFIG_PATH)
	if err != nil {
		return fmt.Errorf("reloading config error: %v", err)
	}

	cfg = cf.ConfYML

	if len(cfg.RDBCfg.Target) <= 0 {
		return errors.New("no found target db setting")
	}

	dbDes = cfg.RDBCfg.Target[0]

	dbsSrc = cfg.RDBCfg.Source
	if len(dbsSrc) == 0 {
		l.Warning("no found source db setting")
	}

	return nil
}

var l *lg.RotateLog
var cfg cf.ConfigYML
var errg error

var dbDes cf.DBDesCfg
var dbsSrc []cf.DBSrcCfg
