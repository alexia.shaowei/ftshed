package cfgreader

func LoadServerConfig(path string) error {

	ConfYML = ConfigYML{}
	err := ReadYAMLConfig(path, &ConfYML)
	if err != nil {
		return err
	}

	return nil
}
