package cfgreader

var ConfYML ConfigYML

type ConfigYML struct {
	RDBCfg     rdbCfg     `yaml:"rdb"`
	CaptureCfg captureCfg `yaml:"capture"`
	ServerCfg  serverCfg  `yaml:"server"`
}

type DBSrcCfg struct {
	Host     string   `yaml:"host"`
	Hostname string   `yaml:"hostname"`
	Port     int      `yaml:"port"`
	User     string   `yaml:"user"`
	Password string   `yaml:"password"`
	Charset  string   `yaml:"charset"`
	Cron     string   `yaml:"cron"`
	Enable   bool     `yaml:"enable"`
	GMTBL    []string `yaml:"gmtbl"`
}

type DBDesCfg struct {
	Host     string `yaml:"host"`
	Hostname string `yaml:"hostname"`
	Port     int    `yaml:"port"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	Charset  string `yaml:"charset"`
}

type rdbCfg struct {
	Source []DBSrcCfg `yaml:"source"`
	Target []DBDesCfg `yaml:"target"`
}

type captureCfg struct {
	Start string `yaml:"start"`
	End   string `yaml:"end"`
}

type serverCfg struct {
	Port string `yaml:"port"`
}
