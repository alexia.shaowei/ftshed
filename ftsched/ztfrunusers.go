package main

import (
	"time"

	sq "future.net.co.ftsched/sqlscript"
	tm "future.net.co.ftsched/tmparse"
	db "gitlab.com/alexia.shaowei/swmysql"

	cf "future.net.co.ftsched/conf"
)

func transRegUsersData(dbSrcCfg cf.DBSrcCfg, dbdes, dbsrc *db.DBOperator) error {
	l.Info("%v::transRegUsersData", dbSrcCfg.Hostname)

	hDate := cfg.CaptureCfg.Start
	tDate := tm.TimeCurrent(tm.CST_TIME_FMT_YMDHMS)
	resp, err := dbdes.Query(sq.QuerySupersetUsersLastDate(dbSrcCfg.Hostname))
	if err != nil {
		return err
	}

	if resp.Length > 0 {
		hDate = tm.TimeOnTheHourNext(resp.RowsResponse[0][cstStatisDateTime], tm.CST_TIME_FMT_YMDHMS, 1)
	}

	dateTime, err := tm.TimeOnTheHourDayInterval(hDate, tDate, tm.CST_TIME_FMT_YMDHMS)
	if err != nil {
		return err
	}

	l.Info("%v::transRegUsersData, hDate: %v, tDate: %v, interval: %v - %v", dbSrcCfg.Hostname, hDate, tDate, dateTime[0]["h"], dateTime[len(dateTime)-1]["t"])

	t1 := time.Now().Unix()
	data := compileUserData(dbSrcCfg.Hostname, dbSrcCfg.GMTBL, dateTime, dbsrc)

	l.Info("%v::transRegUsersData::compileUserData, len: %v, cost: %v sec", dbSrcCfg.Hostname, len(data), time.Now().Unix()-t1)

	if len(data) > 0 {
		sqlSyntax := sq.InsertRegAndActUsers(dbSrcCfg.Hostname, data)
		_, err = dbdes.Exec(sqlSyntax)
		l.Info("%v::transRegUsersData::InsertSupersetStsRecGm, cost: %v sec", dbSrcCfg.Hostname, time.Now().Unix()-t1)
		if err != nil {
			return err
		}
	}

	return nil
}

func compileUserData(dbSrcName string, tbls []string, dateTime []map[string]string, dbsrc *db.DBOperator) []map[string]string {
	data := make([]map[string]string, 0)
	var head string
	var tail string
	var year string
	var mon string

	for _, t := range dateTime {
		head = t["h"]
		tail = t["t"]
		year, mon = tm.TimeGetYM(head, tm.CST_TIME_FMT_YMDHMS)

		tblStatisAllGames := "statis_allgames" + year + mon + "_users"
		resptbl, err := dbsrc.Query(sq.QueryTableIfExist(cstSchemaKYStatisUsers, tblStatisAllGames))
		if err != nil {
			l.Error("%v::compileUserData::QueryTableIfExist() error: %v", dbSrcName, err)
			break
		}

		actUsers := "0"
		if resptbl.Length > 0 {
			r, err := dbsrc.Query(sq.QueryActUsers(tblStatisAllGames, head[:10], head, tail))
			if err != nil {
				l.Error("%v::compileUserData::QueryActUsers(), head: %v, tail: %v, error: %v", dbSrcName, head, tail, err)
				break
			}

			if r.Length > 0 {
				actUsers = r.RowsResponse[0]["ActUsers"]
			}
		}

		respregu, err := dbsrc.Query(sq.QueryRegUsers(head, tail))
		if err != nil {
			l.Error("%v::compileUserData::QueryRegUsers() error: %v", dbSrcName, err)
			break
		}

		mpData := map[string]string{
			"StatisDateTime": head,
			"RegUsers":       respregu.RowsResponse[0]["RegUsers"],
			"ActUsers":       actUsers,
			"SourceDB":       dbSrcName,
		}

		data = append(data, mpData)
	}

	return data
}
