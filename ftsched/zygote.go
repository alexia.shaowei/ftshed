package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/robfig/cron/v3"
)

func main() {
	err := bootCfg()
	if err != nil {
		l.Error(err.Error())
		return
	}

	err = createTable()
	if err != nil {
		l.Error(err.Error())
		return
	}

	c := cron.New()

	srcCfgdbs := cfg.RDBCfg.Source
	for i := 0; i < len(srcCfgdbs); i++ {
		srcCfgdb := srcCfgdbs[i]
		if srcCfgdb.Enable {
			l.Info("add schedule ->  name: %v, host: %v, cron: %v", srcCfgdb.Hostname, srcCfgdb.Host, srcCfgdb.Cron)
			c.AddFunc(srcCfgdb.Cron, func() {
				if err := run(srcCfgdb); err != nil {
					l.Error("zygote run error: %v", err)
				}
			})
		}
	}

	c.Start()

	http.HandleFunc("/superset/sched/version", func(w http.ResponseWriter, r *http.Request) {
		w.Write(CodeResponseSuccess(VERSION))
	})

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", cfg.ServerCfg.Port), nil))
}
