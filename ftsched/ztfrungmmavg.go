package main

import (
	"strconv"
	"strings"
	"time"

	cf "future.net.co.ftsched/conf"
	sq "future.net.co.ftsched/sqlscript"
	tm "future.net.co.ftsched/tmparse"
	db "gitlab.com/alexia.shaowei/swmysql"
)

func transGameMAVGData(dbSrcCfg cf.DBSrcCfg, dbdes, dbsrc *db.DBOperator) error {
	l.Info("%v transGameMAVGData", dbSrcCfg.Hostname)

	hMonth := tm.TimeFMT(cfg.CaptureCfg.Start, tm.CST_TIME_FMT_YMDHMS, tm.CST_TIME_FMT_YM)
	tMonth := tm.TimeCurrent(tm.CST_TIME_FMT_YM)
	lMonth := tMonth

	resp, err := dbdes.Query(sq.QuerySupersetGameMonthAVGLastDate(dbSrcCfg.Hostname))
	if err != nil {
		return err
	}

	if resp.Length > 0 {
		hMonth = resp.RowsResponse[0]["StatisDateMonth"]
	}

	ils, err := tm.TimeOnTheMonthInterval(hMonth, lMonth, tm.CST_TIME_FMT_YM, tm.CST_TIME_FMT_YM)
	if err != nil {
		return err
	}

	for _, m := range ils {

		rspData, err := dbsrc.Query(sq.QueryGameMonthDataSum(m))
		if err != nil {
			return err
		}

		if rspData.Length > 0 {
			days := 1
			if tm.TimeUnixCompare(tMonth, m, tm.CST_TIME_FMT_YM) > 0 {
				yyyymm := strings.Split(m, "-")
				days, _ = tm.TimeMonthDays(yyyymm[1], yyyymm[0])

			} else {
				days = time.Now().Day()
			}

			auh, _ := strconv.ParseFloat(rspData.RowsResponse[0]["ActUserHit"], 64)
			au, _ := strconv.ParseFloat(rspData.RowsResponse[0]["ActiveUsers"], 64)
			cs, _ := strconv.ParseFloat(rspData.RowsResponse[0]["CellScore"], 64)
			hp, _ := strconv.ParseFloat(rspData.RowsResponse[0]["HP"], 64)
			hit, _ := strconv.ParseFloat(rspData.RowsResponse[0]["Hit"], 64)

			avg := float64(days)
			_, err := dbdes.Exec(sq.InsertGameMonthAVG(m, cs/avg, hit/avg, hp/avg, auh/avg, au/avg, dbSrcCfg.Hostname))
			if err != nil {
				return err
			}

		}
	}

	return nil
}
