package main

import (
	cf "future.net.co.ftsched/conf"
	tm "future.net.co.ftsched/tmparse"
	db "gitlab.com/alexia.shaowei/swmysql"
)

func whatTheMess(dbSrcCfg cf.DBSrcCfg) error {
	dbdes, err := db.New(dbDes.User, dbDes.Password, dbDes.Host, dbDes.Port, cstSuperset, dbDes.Charset, 1, 1)
	if err != nil {
		return err
	}

	dbsrc, err := db.New(dbSrcCfg.User, dbSrcCfg.Password, dbSrcCfg.Host, dbSrcCfg.Port, cstDBDefault, dbSrcCfg.Charset, 1, 1)
	if err != nil {
		return err
	}

	defer func() {
		dbdes.Close()
		dbsrc.Close()

		l.Info("whatTheMess() -> dbdes, dbsrc disconnection")
	}()

	hDate := cfg.CaptureCfg.Start
	eDate := cfg.CaptureCfg.End
	tDate := tm.TimeCurrent(tm.CST_TIME_FMT_YMDHMS)
	l.Info("hDate: %v, eDate: %v, tDate: %v", hDate, eDate, tDate)
	if tm.TimeUnixCompare(hDate, tDate, tm.CST_TIME_FMT_YMDHMS) >= 0 {
		l.Warning("hDate %v greater than tDate %v, force done now", hDate, tDate)
		return nil
	}

	return nil
}
