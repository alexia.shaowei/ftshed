package main

const (
	VERSION = "0.0.0"
)

const (
	CONFIG_PATH = "zftconfig.yml"
)

const (
	cstSuperset       = "superset"
	cstKYStatis       = "KYStatis"
	cstDBDefault      = ""
	cstStatisDateTime = "StatisDateTime"
)

const (
	cstSchemaKYStatisUsers = "KYStatisUsers"
	cstOrderByDESC         = "DESC"
	cstOrderByASC          = "ASC"
)
