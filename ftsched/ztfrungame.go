package main

import (
	"time"

	sq "future.net.co.ftsched/sqlscript"
	tm "future.net.co.ftsched/tmparse"
	db "gitlab.com/alexia.shaowei/swmysql"

	cf "future.net.co.ftsched/conf"
)

func transGameData(dbSrcCfg cf.DBSrcCfg, dbdes, dbsrc *db.DBOperator) error {
	l.Info("%v transGameData", dbSrcCfg.Hostname)

	hDate := cfg.CaptureCfg.Start
	tDate := tm.TimeCurrent(tm.CST_TIME_FMT_YMDHMS)
	resp, err := dbdes.Query(sq.QuerySupersetGameLastDate(dbSrcCfg.Hostname))
	if err != nil {
		return err
	}

	if resp.Length > 0 {
		hDate = tm.TimeOnTheHourNext(resp.RowsResponse[0][cstStatisDateTime], tm.CST_TIME_FMT_YMDHMS, 1)
	}

	dateTime, err := tm.TimeOnTheHourDayInterval(hDate, tDate, tm.CST_TIME_FMT_YMDHMS)
	if err != nil {
		return err
	}

	l.Info("%v::transGameData, hDate: %v, tDate: %v, interval: %v - %v", dbSrcCfg.Hostname, hDate, tDate, dateTime[0]["h"], dateTime[len(dateTime)-1]["t"])

	t1 := time.Now().Unix()
	data := compileGameData(dbSrcCfg.Hostname, dbSrcCfg.GMTBL, dateTime, dbsrc)
	l.Info("%v::transGameData::compileGameData, len: %v, cost: %v sec", dbSrcCfg.Hostname, len(data), time.Now().Unix()-t1)

	if len(data) > 0 {
		sqlSyntax := sq.InsertSupersetStsRecGm(data)

		t1 = time.Now().Unix()
		_, err := dbdes.Exec(sqlSyntax)
		l.Info("%v::transGameData::InsertSupersetStsRecGm, cost: %v sec", dbSrcCfg.Hostname, time.Now().Unix()-t1)
		if err != nil {
			return err
		}
	}

	return nil
}

func compileGameData(dbSrcName string, tbls []string, dataTime []map[string]string, dbsrc *db.DBOperator) []map[string]string {
	data := make([]map[string]string, 0)

	var head string
	var tail string
	var day string
	var tblSchema string
	var tblName string

	for _, gm := range tbls {
		tblSchema = gm + "_record"
		l.Info("dbName: %v, tblSchema: %v", dbSrcName, gm)
		for _, time := range dataTime {
			head = time["h"]
			tail = time["t"]
			day = tm.TimeFMT(head, tm.CST_TIME_FMT_YMDHMS, tm.CST_TIME_FMT_YMD_CMP)
			tblName = "gameRecord" + day

			respTbl, err := dbsrc.Query(sq.QueryTableIfExist(tblSchema, tblName))
			if err != nil {
				l.Error("%v::compileGameData::QueryTableIfExist, error: %v", dbSrcName, err)
				break
			}

			if respTbl.Length <= 0 {
				continue
			}

			respGm, err := dbsrc.Query(sq.QuerySrcGmRec(gm+"_record."+respTbl.RowsResponse[0]["TABLE_NAME"], head, tail))
			if err != nil {
				l.Error("%v::compileGameData::QuerySrcGmRec, headTime: %v, tailTime: %v, error: %v", dbSrcName, head, tail, err)
				break
			}

			if respGm.Length > 0 {
				dt := map[string]string{
					"StatisDateTime": head,
					"ActiveUsers":    respGm.RowsResponse[0]["ActiveUsers"],
					"CellScore":      respGm.RowsResponse[0]["CellScore"],
					"ChannelID":      respGm.RowsResponse[0]["ChannelID"],
					"GameID":         respGm.RowsResponse[0]["GameID"],
					"LostGold":       respGm.RowsResponse[0]["LostGold"],
					"LostNum":        respGm.RowsResponse[0]["LostNum"],
					"Revenue":        respGm.RowsResponse[0]["Revenue"],
					"WinGold":        respGm.RowsResponse[0]["WinGold"],
					"WinNum":         respGm.RowsResponse[0]["WinNum"],
					"SourceDB":       dbSrcName,
				}

				data = append(data, dt)
			}
		}
	}

	return data
}
