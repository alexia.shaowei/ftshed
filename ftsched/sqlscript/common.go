package sqlscritp

import (
	"fmt"
	"strings"
)

func QueryTableIfExist(tblSchema, tblName string) string {
	var sb strings.Builder

	sb.WriteString("SELECT tbl.TABLE_NAME AS TABLE_NAME FROM information_schema.TABLES tbl ")
	sb.WriteString(fmt.Sprintf("WHERE tbl.TABLE_SCHEMA = '%s' ", tblSchema))
	sb.WriteString(fmt.Sprintf("AND tbl.TABLE_NAME = '%s';", tblName))

	return sb.String()
}
