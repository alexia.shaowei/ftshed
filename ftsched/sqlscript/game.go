package sqlscritp

import (
	"fmt"
	"strings"

	"gitlab.com/alexia.shaowei/swmysql"
)

func CreateTableGamesPerHour() string {
	var sb strings.Builder

	sb.WriteString("CREATE TABLE IF NOT EXISTS `statis_record_game_per_hour` (")
	sb.WriteString(" `StatisDateTime` datetime NOT NULL,")
	sb.WriteString(" `ChannelID` int(11) NOT NULL,")
	sb.WriteString(" `GameID` int(11) NOT NULL,")
	sb.WriteString(" `WinGold` bigint(20) DEFAULT '0',")
	sb.WriteString(" `LostGold` bigint(20) DEFAULT '0',")
	sb.WriteString(" `CellScore` bigint(20) DEFAULT '0',")
	sb.WriteString(" `Revenue` bigint(20) DEFAULT '0',")
	sb.WriteString(" `WinNum` int(11) DEFAULT '0',")
	sb.WriteString(" `LostNum` int(11) DEFAULT '0',")
	sb.WriteString(" `ActiveUsers` int(11) DEFAULT '0',")
	sb.WriteString(" `SourceDB` varchar(20) NOT NULL,")
	sb.WriteString(" `MoveDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,")
	sb.WriteString(" PRIMARY KEY (`StatisDateTime`, `ChannelID`,`GameID`, `SourceDB`)")
	sb.WriteString(") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;")

	return sb.String()
}

func QuerySupersetGameLastDate(dbHost string) string {
	var sb strings.Builder

	sb.WriteString("SELECT sragph.StatisDateTime ")
	sb.WriteString("FROM superset.statis_record_game_per_hour sragph ")
	sb.WriteString("WHERE sragph.SourceDB = '" + dbHost + "' ")
	sb.WriteString("ORDER BY sragph.StatisDateTime DESC ")
	sb.WriteString("LIMIT 1;")

	return sb.String()
}

func QuerySrcGmRec(srcTable, startTime, endTime string) string {

	var sb strings.Builder
	sb.WriteString("SELECT")
	sb.WriteString(" gr.KindID AS 'GameID',")
	sb.WriteString(" gr.ChannelID,")
	sb.WriteString(" SUM(gr.Revenue) AS Revenue,")
	sb.WriteString(" SUM(gr.CellScore) AS CellScore,")
	sb.WriteString(" SUM(CASE WHEN   gr.Profit >  0 THEN gr.Profit ELSE 0 END) AS WinGold,")
	sb.WriteString(" SUM(CASE WHEN   gr.Profit <  0 THEN gr.Profit ELSE 0 END) AS LostGold,")
	sb.WriteString(" COUNT(CASE WHEN gr.Profit >= 0 THEN gr.Profit END) WinNum,")
	sb.WriteString(" COUNT(CASE WHEN gr.Profit <  0 THEN gr.Profit END) LostNum,")
	sb.WriteString(" COUNT(DISTINCT gr.Accounts) AS ActiveUsers ")
	sb.WriteString("FROM " + srcTable + " gr ")
	sb.WriteString("WHERE gr.GameStartTime >= '" + startTime + "' ")
	sb.WriteString("AND   gr.GameEndTime   <= '" + endTime + "' ")
	sb.WriteString("GROUP BY gr.ChannelID;")

	return sb.String()
}

func ReplaceSupersetStsRecGm(sourceDB, statisDateTime string, selResponse *swmysql.DBResponse) string {

	var sb strings.Builder
	sb.WriteString("REPLACE INTO superset.statis_record_game_per_hour (")
	sb.WriteString(" StatisDateTime,")
	sb.WriteString(" ChannelID,")
	sb.WriteString(" GameID,")
	sb.WriteString(" WinGold,")
	sb.WriteString(" LostGold,")
	sb.WriteString(" CellScore,")
	sb.WriteString(" Revenue,")
	sb.WriteString(" WinNum,")
	sb.WriteString(" LostNum,")
	sb.WriteString(" ActiveUsers,")
	sb.WriteString(" SourceDB")
	sb.WriteString(")")
	sb.WriteString("VALUES")
	for i := 0; i < int(selResponse.Length); i++ {
		syntax := fmt.Sprintf("( '%v', %v, %v, %v, %v, %v, %v, %v, %v, %v, '%v'),",
			statisDateTime,
			selResponse.RowsResponse[i]["ChannelID"],
			selResponse.RowsResponse[i]["GameID"],
			selResponse.RowsResponse[i]["WinGold"],
			selResponse.RowsResponse[i]["LostGold"],
			selResponse.RowsResponse[i]["CellScore"],
			selResponse.RowsResponse[i]["Revenue"],
			selResponse.RowsResponse[i]["WinNum"],
			selResponse.RowsResponse[i]["LostNum"],
			selResponse.RowsResponse[i]["ActiveUsers"],
			sourceDB,
		)

		sb.WriteString(syntax)
	}

	rtn := sb.String()[:len(sb.String())-1]

	return rtn + ";"
}

func InsertSupersetStsRecGm(data []map[string]string) string {

	var sb strings.Builder
	sb.WriteString("REPLACE INTO superset.statis_record_game_per_hour (")
	sb.WriteString(" StatisDateTime,")
	sb.WriteString(" ChannelID,")
	sb.WriteString(" GameID,")
	sb.WriteString(" WinGold,")
	sb.WriteString(" LostGold,")
	sb.WriteString(" CellScore,")
	sb.WriteString(" Revenue,")
	sb.WriteString(" WinNum,")
	sb.WriteString(" LostNum,")
	sb.WriteString(" ActiveUsers,")
	sb.WriteString(" SourceDB")
	sb.WriteString(")")
	sb.WriteString("VALUES")

	for _, val := range data {

		if len(val["ChannelID"]) == 0 || len(val["GameID"]) == 0 {
			continue
		}

		syntax := fmt.Sprintf("('%v', %v, %v, %v, %v, %v, %v, %v, %v, %v, '%v'),",
			val["StatisDateTime"],
			val["ChannelID"],
			val["GameID"],
			val["WinGold"],
			val["LostGold"],
			val["CellScore"],
			val["Revenue"],
			val["WinNum"],
			val["LostNum"],
			val["ActiveUsers"],
			val["SourceDB"],
		)

		sb.WriteString(syntax)
	}

	rtn := sb.String()[:len(sb.String())-1]

	return rtn + ";"
}

func QuerySupersetGamePerDate(dbHost, orderBy string) string {
	var sb strings.Builder

	sb.WriteString("SELECT sragph.StatisDateTime ")
	sb.WriteString("FROM superset.statis_record_game_per_hour sragph ")
	sb.WriteString("WHERE sragph.SourceDB = '" + dbHost + "' ")
	sb.WriteString("ORDER BY sragph.StatisDateTime " + orderBy)
	sb.WriteString(" LIMIT 1;")

	return sb.String()
}
