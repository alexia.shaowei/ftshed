package sqlscritp

import (
	"fmt"
	"strings"
)

func CreateTableUsersPerHour() string {
	var sb strings.Builder

	sb.WriteString("CREATE TABLE IF NOT EXISTS `statis_record_users_per_hour` (")
	sb.WriteString(" `StatisDateTime` datetime NOT NULL,")
	sb.WriteString(" `RegUsers` int(11) DEFAULT '0',")
	sb.WriteString(" `ActUsers` int(11) DEFAULT '0',")
	sb.WriteString(" `SourceDB` varchar(20) NOT NULL,")
	sb.WriteString(" `MoveDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,")
	sb.WriteString(" PRIMARY KEY (`StatisDateTime`, `SourceDB`)")
	sb.WriteString(") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;")

	return sb.String()
}

func QuerySupersetUsersLastDate(dbHost string) string {
	var sb strings.Builder

	sb.WriteString("SELECT sruph.StatisDateTime ")
	sb.WriteString("FROM superset.statis_record_users_per_hour sruph ")
	sb.WriteString("WHERE sruph.SourceDB = '" + dbHost + "' ")
	sb.WriteString("ORDER BY sruph.StatisDateTime DESC ")
	sb.WriteString("LIMIT 1;")

	return sb.String()
}

func QueryRegUsers(headTime, tailTime string) string {
	var sb strings.Builder

	sb.WriteString("SELECT COUNT(acnt.account) AS RegUsers FROM KYDB_NEW.accounts acnt ")
	sb.WriteString("WHERE acnt.createdate >= '" + headTime + "' ")
	sb.WriteString("AND acnt.createdate <= '" + tailTime + "';")

	return sb.String()
}

func QueryActUsers(srcTbl, statisDate, headTime, tailTime string) string {
	var sb strings.Builder
	sb.WriteString("SELECT COUNT(DISTINCT algm.Account) AS ActUsers ")
	sb.WriteString("FROM KYStatisUsers." + srcTbl + " algm WHERE algm.StatisDate = '" + statisDate + "' ")
	sb.WriteString(" AND algm.Account IN ( ")
	sb.WriteString(" SELECT na.account FROM KYDB_NEW.accounts na ")
	sb.WriteString("WHERE na.createdate >= '" + headTime + "' AND na.createdate <= '" + tailTime + "');")

	return sb.String()
}

func ReplaceRegAndActUsers(sourceDB string, data []map[string]string) string {
	var sb strings.Builder

	sb.WriteString("Replace INTO superset.statis_record_users_per_hour (")
	sb.WriteString(" StatisDateTime,")
	sb.WriteString(" RegUsers,")
	sb.WriteString(" ActUsers,")
	sb.WriteString(" SourceDB")
	sb.WriteString(")")
	sb.WriteString("VALUES")
	for i := 0; i < len(data); i++ {
		syntax := fmt.Sprintf("( '%v', %v, %v, '%v'),",
			data[i]["StatisDateTime"],
			data[i]["RegUsers"],
			data[i]["ActUsers"],
			sourceDB,
		)

		sb.WriteString(syntax)
	}

	rtn := sb.String()[:len(sb.String())-1]

	return rtn + ";"
}

func InsertRegAndActUsers(sourceDB string, data []map[string]string) string {
	var sb strings.Builder

	sb.WriteString("INSERT INTO superset.statis_record_users_per_hour (")
	sb.WriteString(" StatisDateTime,")
	sb.WriteString(" RegUsers,")
	sb.WriteString(" ActUsers,")
	sb.WriteString(" SourceDB")
	sb.WriteString(")")
	sb.WriteString("VALUES")
	for i := 0; i < len(data); i++ {
		syntax := fmt.Sprintf("('%v', %v, %v, '%v'),",
			data[i]["StatisDateTime"],
			data[i]["RegUsers"],
			data[i]["ActUsers"],
			sourceDB,
		)

		sb.WriteString(syntax)
	}

	rtn := sb.String()[:len(sb.String())-1]

	return rtn + ";"
}
