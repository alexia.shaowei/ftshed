package sqlscritp

import (
	"fmt"
	"strings"
)

func CreateTableGameMonthAVG() string {
	var sb strings.Builder

	sb.WriteString("CREATE TABLE IF NOT EXISTS `statis_record_agent_game_mon_avg` (")
	sb.WriteString("  `StatisDateMonth` VARCHAR(7) NOT NULL,")
	sb.WriteString("  `CellScoreMAvg` DECIMAL(30, 2) DEFAULT '0' COMMENT '日均總血量',")
	sb.WriteString("  `HitMAvg` DECIMAL(30, 2) DEFAULT '0' COMMENT '日均傷害',")
	sb.WriteString("  `HPMAvg` DECIMAL(30, 2) DEFAULT '0' COMMENT '日人均總血量',")
	sb.WriteString("  `ActUserHitMAvg` DECIMAL(30, 2) DEFAULT '0' COMMENT '日人均傷害',")
	sb.WriteString("  `ActUserMAvg` INT(20) DEFAULT '0' COMMENT '日均超神人數',")
	sb.WriteString("  `SourceDB` VARCHAR(20) NOT NULL,")
	sb.WriteString("  `MoveDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,")
	sb.WriteString(" PRIMARY KEY (`StatisDateMonth`,`SourceDB`)")
	sb.WriteString(") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;")

	return sb.String()
}

func InsertGameMonthAVG(StatisDateMonth string, CellScoreMAvg, HitMAvg, HPMAvg, ActUserHitMAvg, ActUserMAvg float64, SourceDB string) string {
	var sb strings.Builder

	sb.WriteString("REPLACE INTO superset.statis_record_agent_game_mon_avg (StatisDateMonth, CellScoreMAvg, HitMAvg, HPMAvg, ActUserHitMAvg, ActUserMAvg, SourceDB)")
	sb.WriteString(fmt.Sprintf("VALUES('%v', %v, %v, %v, %v, %v, '%v');", StatisDateMonth, CellScoreMAvg, HitMAvg, HPMAvg, ActUserHitMAvg, ActUserMAvg, SourceDB))

	return sb.String()
}

func UpdateGameMonthAVG(StatisDateMonth string, CellScoreMAvg, HitMAvg, HPMAvg, ActUserHitMAvg, ActUserMAvg float64, SourceDB string) string {
	var sb strings.Builder

	sb.WriteString("UPDATE superset.statis_record_agent_game_mon_avg SET ")
	sb.WriteString(fmt.Sprintf("CellScoreMAvg = %v, HitMAvg = %v, HPMAvg = %v, ActUserHitMAvg = %v, ActUserMAvg = %v", CellScoreMAvg, HitMAvg, HPMAvg, ActUserHitMAvg, ActUserMAvg))
	sb.WriteString(", UpdateTime = CURRENT_TIMESTAMP ")
	sb.WriteString("WHERE StatisDateMonth = '" + StatisDateMonth + "' AND SourceDB = '" + SourceDB + "';")

	return sb.String()
}

func QuerySupersetGameMonthAVGLastDate(sourceDB string) string {
	var sb strings.Builder

	sb.WriteString("SELECT sragma.StatisDateMonth ")
	sb.WriteString("FROM superset.statis_record_agent_game_mon_avg sragma ")
	sb.WriteString("WHERE sragma.SourceDB = '" + sourceDB + "'")
	sb.WriteString("ORDER BY sragma.StatisDateMonth DESC ")
	sb.WriteString("LIMIT 1;")

	return sb.String()
}

func QueryGameMonthDataSum(yyyymm string) string {
	var sb strings.Builder

	sb.WriteString("SELECT ")
	sb.WriteString(" SUM(srag.CellScore) AS CellScore,")
	sb.WriteString(" SUM(srag.ActiveUsers) AS ActiveUsers,")
	sb.WriteString(" SUM(srag.CellScore)/SUM(srag.ActiveUsers) AS HP,")
	sb.WriteString(" -(SUM(srag.WinGold)+SUM(srag.LostGold)) AS Hit,")
	sb.WriteString(" -(SUM(srag.WinGold)+SUM(srag.LostGold))/SUM(srag.ActiveUsers) AS ActUserHit ")
	sb.WriteString("FROM KYStatis.statis_record_agent_game srag ")
	sb.WriteString("WHERE srag.StatisDate LIKE '" + yyyymm + "%';")

	return sb.String()
}
