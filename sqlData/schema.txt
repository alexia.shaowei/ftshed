CREATE DATABASE `KYStatis` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

CREATE TABLE `statis_record_agent_game` (
  `StatisDate` date NOT NULL,
  `ChannelID` int(11) NOT NULL,
  `GameID` int(11) NOT NULL,
  `WinGold` bigint(20) DEFAULT '0' COMMENT '赢钱',
  `LostGold` bigint(20) DEFAULT '0' COMMENT '输钱',
  `CellScore` bigint(20) DEFAULT '0' COMMENT '有效投注',
  `Revenue` bigint(20) DEFAULT '0' COMMENT '抽水',
  `WinNum` int(11) DEFAULT '0' COMMENT '赢钱局数',
  `LostNum` int(11) DEFAULT '0' COMMENT '输钱局数',
  `ActiveUsers` int(11) DEFAULT '0',
  `AccumulateGameTime` bigint(20) DEFAULT '0' COMMENT '累積遊玩時間(秒)',
  `MaxCellScore` bigint(20) DEFAULT '0' COMMENT '單局最高投注',
  PRIMARY KEY (`StatisDate`,`ChannelID`,`GameID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

